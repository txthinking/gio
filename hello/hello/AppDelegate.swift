//
//  AppDelegate.swift
//  hello
//
//  Created by tx on 6/23/20.
//  Copyright © 2020 hello. All rights reserved.
//

import UIKit
import Gophers

@UIApplicationMain
class AppDelegate: GioAppDelegate {

    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return super.application(application, didFinishLaunchingWithOptions: launchOptions);
    }

}

